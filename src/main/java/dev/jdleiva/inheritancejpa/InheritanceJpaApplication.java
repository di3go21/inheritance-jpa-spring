package dev.jdleiva.inheritancejpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InheritanceJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(InheritanceJpaApplication.class, args);
    }

}